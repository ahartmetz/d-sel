/*
   Copyright (C) 2013 Andreas Hartmetz <ahartmetz@gmail.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this library; see the file COPYING.LGPL.  If not, write to
   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.

   Alternatively, this file is available under the Mozilla Public License
   Version 1.1.  You may obtain a copy of the License at
   http://www.mozilla.org/MPL/
*/

#include "argumentlist.h"

#include "../testutil.h"

#include <algorithm>
#include <cstring>
#include <iostream>

// Handy helpers

static void printChunk(chunk a)
{
    std::cout << "Array: ";
    for (int i = 0; i < a.length; i++) {
        std::cout << int(a.begin[i]) << '|';
    }
    std::cout << '\n';
}

static bool chunksEqual(chunk a1, chunk a2)
{
    if (a1.length != a2.length) {
        std::cout << "Different lengths.\n";
        printChunk(a1);
        printChunk(a2);
        return false;
    }
    for (int i = 0; i < a1.length; i++) {
        if (a1.begin[i] != a2.begin[i]) {
            std::cout << "Different content.\n";
            printChunk(a1);
            printChunk(a2);
            return false;
        }
    }
    return true;
}

static bool stringsEqual(cstring s1, cstring s2)
{
    return chunksEqual(chunk(s1.begin, s1.length), chunk(s2.begin, s2.length));
}

static void doRoundtrip(ArgumentList arg_in, bool skipNextEntryAtArrayStart, int dataIncrement, bool debugPrint)
{
    cstring signature = arg_in.signature();
    chunk data = arg_in.data();
    chunk shortData;

    ArgumentList arg(signature, shortData);

    ArgumentList::Reader reader = arg.beginRead();

    ArgumentList copy;
    ArgumentList::Writer writer = copy.beginWrite();

    bool isDone = false;
    int emptyNesting = 0;
    bool isFirstEntry = false;

    while (!isDone) {
        TEST(writer.state() != ArgumentList::InvalidData);
        if (debugPrint) {
            std::cout << "Reader state: " << reader.stateString().begin << '\n';
        }

        switch(reader.state()) {
        case ArgumentList::Finished:
            writer.finish();
            isDone = true;
            break;
        case ArgumentList::NeedMoreData: {
            TEST(shortData.length < data.length);
            // reallocate shortData to test that Reader can handle the data moving around - and
            // allocate the new one before destroying the old one to make sure that the pointer differs
            chunk oldData = shortData;
            shortData.length = std::min(shortData.length + dataIncrement, data.length);
            shortData.begin = reinterpret_cast<byte *>(malloc(shortData.length));
            for (int i = 0; i < shortData.length; i++) {
                shortData.begin[i] = data.begin[i];
            }
            // clobber it to provoke errors that only valgrind might find otherwise
            for (int i = 0; i < oldData.length; i++) {
                oldData.begin[i] = '\xff';
            }
            if (oldData.begin) {
                free(oldData.begin);
            }
            reader.replaceData(shortData);
            break; }
        case ArgumentList::BeginStruct:
            reader.beginStruct();
            writer.beginStruct();
            break;
        case ArgumentList::EndStruct:
            reader.endStruct();
            writer.endStruct();
            break;
        case ArgumentList::BeginVariant:
            reader.beginVariant();
            writer.beginVariant();
            break;
        case ArgumentList::EndVariant:
            reader.endVariant();
            writer.endVariant();
            break;
        case ArgumentList::BeginArray: {
            isFirstEntry = true;
            bool isEmpty;
            reader.beginArray(&isEmpty);
            writer.beginArray(isEmpty);
            emptyNesting += isEmpty ? 1 : 0;
            break; }
        case ArgumentList::NextArrayEntry:
            if (reader.nextArrayEntry()) {
                if (isFirstEntry && skipNextEntryAtArrayStart) {
                    isFirstEntry = false;
                } else {
                    writer.nextArrayEntry();
                }
            }
            break;
        case ArgumentList::EndArray:
            reader.endArray();
            writer.endArray();
            emptyNesting = std::max(emptyNesting - 1, 0);
            break;
        case ArgumentList::BeginDict: {
            isFirstEntry = true;
            bool isEmpty;
            reader.beginDict(&isEmpty);
            writer.beginDict(isEmpty);
            emptyNesting += isEmpty ? 1 : 0;
            break; }
        case ArgumentList::NextDictEntry:
            if (reader.nextDictEntry()) {
                if (isFirstEntry && skipNextEntryAtArrayStart) {
                    isFirstEntry = false;
                } else {
                    writer.nextDictEntry();
                }
            }
            break;
        case ArgumentList::EndDict:
            reader.endDict();
            writer.endDict();
            emptyNesting = std::max(emptyNesting - 1, 0);
            break;
        case ArgumentList::Byte:
            writer.writeByte(reader.readByte());
            break;
        case ArgumentList::Boolean:
            writer.writeBoolean(reader.readBoolean());
            break;
        case ArgumentList::Int16:
            writer.writeInt16(reader.readInt16());
            break;
        case ArgumentList::Uint16:
            writer.writeUint16(reader.readUint16());
            break;
        case ArgumentList::Int32:
            writer.writeInt32(reader.readInt32());
            break;
        case ArgumentList::Uint32:
            writer.writeUint32(reader.readUint32());
            break;
        case ArgumentList::Int64:
            writer.writeInt64(reader.readInt64());
            break;
        case ArgumentList::Uint64:
            writer.writeUint64(reader.readUint64());
            break;
        case ArgumentList::Double:
            writer.writeDouble(reader.readDouble());
            break;
        case ArgumentList::String: {
            cstring s = reader.readString();
            if (emptyNesting) {
                s = cstring("");
            } else {
                TEST(ArgumentList::isStringValid(s));
            }
            writer.writeString(s);
            break; }
        case ArgumentList::ObjectPath: {
            cstring objectPath = reader.readObjectPath();
            if (emptyNesting) {
                objectPath = cstring("/");
            } else {
                TEST(ArgumentList::isObjectPathValid(objectPath));
            }
            writer.writeObjectPath(objectPath);
            break; }
        case ArgumentList::Signature: {
            cstring signature = reader.readSignature();
            if (emptyNesting) {
                signature = cstring("");
            } else {
                TEST(ArgumentList::isSignatureValid(signature));
            }
            writer.writeSignature(signature);
            break; }
        case ArgumentList::UnixFd:
            writer.writeUnixFd(reader.readUnixFd());
            break;
        default:
            TEST(false);
            break;
        }
    }
    cstring argSignature = arg.signature();
    cstring copySignature = copy.signature();
    TEST(ArgumentList::isSignatureValid(copySignature));
    TEST(stringsEqual(argSignature, copySignature));

    // TODO when it's wired up between Reader and ArgumentList: chunk argData = arg.data();
    chunk argData = arg_in.data();

    chunk copyData = copy.data();
    TEST(argData.length == copyData.length);
    if (debugPrint && !chunksEqual(argData, copyData)) {
        printChunk(argData);
        printChunk(copyData);
    }
    TEST(chunksEqual(argData, copyData));

    if (shortData.begin) {
        free(shortData.begin);
    }
    free(copySignature.begin);
    free(copyData.begin);
}

static void doRoundtrip(ArgumentList arg, bool debugPrint = false)
{
    int maxIncrement = arg.data().length;
    for (int i = 1; i <= maxIncrement; i++) {
        doRoundtrip(arg, false, i, debugPrint);
        doRoundtrip(arg, true, i, debugPrint);
    }
}



// Tests proper



static void test_stringValidation()
{
    {
        cstring emptyWithNull("");
        cstring emptyWithoutNull;

        TEST(!ArgumentList::isStringValid(emptyWithoutNull));
        TEST(ArgumentList::isStringValid(emptyWithNull));

        TEST(!ArgumentList::isObjectPathValid(emptyWithoutNull));
        TEST(!ArgumentList::isObjectPathValid(emptyWithNull));

        TEST(ArgumentList::isSignatureValid(emptyWithNull));
        TEST(!ArgumentList::isSignatureValid(emptyWithoutNull));
        TEST(!ArgumentList::isSignatureValid(emptyWithNull, ArgumentList::VariantSignature));
        TEST(!ArgumentList::isSignatureValid(emptyWithoutNull, ArgumentList::VariantSignature));
    }
    {
        cstring trivial("i");
        TEST(ArgumentList::isSignatureValid(trivial));
        TEST(ArgumentList::isSignatureValid(trivial, ArgumentList::VariantSignature));
    }
    {
        cstring list("iqb");
        TEST(ArgumentList::isSignatureValid(list));
        TEST(!ArgumentList::isSignatureValid(list, ArgumentList::VariantSignature));
        cstring list2("aii");
        TEST(ArgumentList::isSignatureValid(list2));
        TEST(!ArgumentList::isSignatureValid(list2, ArgumentList::VariantSignature));
    }
    {
        cstring simpleArray("ai");
        TEST(ArgumentList::isSignatureValid(simpleArray));
        TEST(ArgumentList::isSignatureValid(simpleArray, ArgumentList::VariantSignature));
    }
    {
        cstring messyArray("a(iaia{ia{iv}})");
        TEST(ArgumentList::isSignatureValid(messyArray));
        TEST(ArgumentList::isSignatureValid(messyArray, ArgumentList::VariantSignature));
    }
    {
        cstring dictFail("a{vi}");
        TEST(!ArgumentList::isSignatureValid(dictFail));
        TEST(!ArgumentList::isSignatureValid(dictFail, ArgumentList::VariantSignature));
    }
    {
        cstring emptyStruct("()");
        TEST(!ArgumentList::isSignatureValid(emptyStruct));
        TEST(!ArgumentList::isSignatureValid(emptyStruct, ArgumentList::VariantSignature));
        cstring emptyStruct2("(())");
        TEST(!ArgumentList::isSignatureValid(emptyStruct2));
        TEST(!ArgumentList::isSignatureValid(emptyStruct2, ArgumentList::VariantSignature));
        cstring miniStruct("(t)");
        TEST(ArgumentList::isSignatureValid(miniStruct));
        TEST(ArgumentList::isSignatureValid(miniStruct, ArgumentList::VariantSignature));
        cstring badStruct("((i)");
        TEST(!ArgumentList::isSignatureValid(badStruct));
        TEST(!ArgumentList::isSignatureValid(badStruct, ArgumentList::VariantSignature));
        cstring badStruct2("(i))");
        TEST(!ArgumentList::isSignatureValid(badStruct2));
        TEST(!ArgumentList::isSignatureValid(badStruct2, ArgumentList::VariantSignature));
    }
    {
        cstring nullStr;
        cstring emptyStr("");
        TEST(!ArgumentList::isObjectPathValid(nullStr));
        TEST(!ArgumentList::isObjectPathValid(emptyStr));
        TEST(ArgumentList::isObjectPathValid(cstring("/")));
        TEST(!ArgumentList::isObjectPathValid(cstring("/abc/")));
        TEST(ArgumentList::isObjectPathValid(cstring("/abc")));
        TEST(ArgumentList::isObjectPathValid(cstring("/abc/def")));
        TEST(!ArgumentList::isObjectPathValid(cstring("/abc&def")));
        TEST(!ArgumentList::isObjectPathValid(cstring("/abc//def")));
        TEST(ArgumentList::isObjectPathValid(cstring("/aZ/0123_zAZa9_/_")));
    }
    {
        cstring maxStruct("((((((((((((((((((((((((((((((((i"
                          "))))))))))))))))))))))))))))))))");
        TEST(ArgumentList::isSignatureValid(maxStruct));
        TEST(ArgumentList::isSignatureValid(maxStruct, ArgumentList::VariantSignature));
        cstring struct33("(((((((((((((((((((((((((((((((((i" // too much nesting by one
                         ")))))))))))))))))))))))))))))))))");
        TEST(!ArgumentList::isSignatureValid(struct33));
        TEST(!ArgumentList::isSignatureValid(struct33, ArgumentList::VariantSignature));

        cstring maxArray("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaai");
        TEST(ArgumentList::isSignatureValid(maxArray));
        TEST(ArgumentList::isSignatureValid(maxArray, ArgumentList::VariantSignature));
        cstring array33("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaai");
        TEST(!ArgumentList::isSignatureValid(array33));
        TEST(!ArgumentList::isSignatureValid(array33, ArgumentList::VariantSignature));
    }
}

static void test_readerWriterExclusion()
{
    ArgumentList arg;
    {
        ArgumentList::Reader reader1 = arg.beginRead();
        {
            ArgumentList::Reader reader2 = arg.beginRead();
            TEST(reader2.isValid());
        }
        {
            ArgumentList::Writer writer1 = arg.beginWrite();
            TEST(!writer1.isValid());
        }
    }
    {
        ArgumentList::Reader reader3 = arg.beginRead();
        TEST(reader3.isValid());
    }
    {
        ArgumentList::Writer writer2 = arg.beginWrite();
        TEST(writer2.isValid());
        {
            ArgumentList::Reader reader4 = arg.beginRead();
            TEST(!reader4.isValid());
        }
        {
            ArgumentList::Reader writer3 = arg.beginRead();
            TEST(!writer3.isValid());
        }
    }
    {
        ArgumentList::Writer writer4 = arg.beginWrite();
        TEST(writer4.isValid());
    }
}

static void test_nesting()
{
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        for (int i = 0; i < 32; i++) {
            writer.beginArray(false);
            writer.nextArrayEntry();
        }
        TEST(writer.state() != ArgumentList::InvalidData);
        writer.beginArray(false);
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        for (int i = 0; i < 32; i++) {
            writer.beginDict(false);
            writer.nextDictEntry();
            writer.writeInt32(i); // key, next nested dict is value
        }
        TEST(writer.state() != ArgumentList::InvalidData);
        writer.beginStruct();
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        for (int i = 0; i < 32; i++) {
            writer.beginDict(false);
            writer.nextDictEntry();
            writer.writeInt32(i); // key, next nested dict is value
        }
        TEST(writer.state() != ArgumentList::InvalidData);
        writer.beginArray(false);
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        for (int i = 0; i < 64; i++) {
            writer.beginVariant();
        }
        TEST(writer.state() != ArgumentList::InvalidData);
        writer.beginVariant();
        TEST(writer.state() == ArgumentList::InvalidData);
    }
}

struct LengthPrefixedData
{
    uint32 length;
    byte data[256];
};

static void test_roundtrip()
{
    doRoundtrip(ArgumentList(cstring(""), chunk()));
    {
        byte data[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        doRoundtrip(ArgumentList(cstring("i"), chunk(data, 4)));
        doRoundtrip(ArgumentList(cstring("yyyy"), chunk(data, 4)));
        doRoundtrip(ArgumentList(cstring("iy"), chunk(data, 5)));
        doRoundtrip(ArgumentList(cstring("iiy"), chunk(data, 9)));
        doRoundtrip(ArgumentList(cstring("nquy"), chunk(data, 9)));
        doRoundtrip(ArgumentList(cstring("unqy"), chunk(data, 9)));
        doRoundtrip(ArgumentList(cstring("nqy"), chunk(data, 5)));
        doRoundtrip(ArgumentList(cstring("qny"), chunk(data, 5)));
        doRoundtrip(ArgumentList(cstring("yyny"), chunk(data, 5)));
        doRoundtrip(ArgumentList(cstring("qyyy"), chunk(data, 5)));
        doRoundtrip(ArgumentList(cstring("d"), chunk(data, 8)));
        doRoundtrip(ArgumentList(cstring("dy"), chunk(data, 9)));
        doRoundtrip(ArgumentList(cstring("x"), chunk(data, 8)));
        doRoundtrip(ArgumentList(cstring("xy"), chunk(data, 9)));
        doRoundtrip(ArgumentList(cstring("t"), chunk(data, 8)));
        doRoundtrip(ArgumentList(cstring("ty"), chunk(data, 9)));
    }
    {
        LengthPrefixedData testArray = {0};
        for (int i = 0; i < 64; i++) {
            testArray.data[i] = i;
        }
        byte *testData = reinterpret_cast<byte *>(&testArray);

        testArray.length = 1;
        doRoundtrip(ArgumentList(cstring("ay"), chunk(testData, 5)));
        testArray.length = 4;
        doRoundtrip(ArgumentList(cstring("ai"), chunk(testData, 8)));
        testArray.length = 8;
        doRoundtrip(ArgumentList(cstring("ai"), chunk(testData, 12)));
        testArray.length = 64;
        doRoundtrip(ArgumentList(cstring("ai"), chunk(testData, 68)));
        doRoundtrip(ArgumentList(cstring("an"), chunk(testData, 68)));

        testArray.data[0] = 0; testArray.data[1] = 0; // zero out padding
        testArray.data[2] = 0; testArray.data[3] = 0;
        testArray.length = 56;
        doRoundtrip(ArgumentList(cstring("ad"), chunk(testData, 64)));
    }
    {
        LengthPrefixedData testString;
        for (int i = 0; i < 200; i++) {
            testString.data[i] = 'A' + i % 53; // stay in the 7-bit ASCII range
        }
        testString.data[200] = '\0';
        testString.length = 200;
        byte *testData = reinterpret_cast<byte *>(&testString);
        doRoundtrip(ArgumentList(cstring("s"), chunk(testData, 205)));
    }
    {
        LengthPrefixedData testDict;
        testDict.length = 2;
        testDict.data[0] = 0; testDict.data[1] = 0; // zero padding; dict entries are always 8-aligned.
        testDict.data[2] = 0; testDict.data[3] = 0;

        testDict.data[4] = 23;
        testDict.data[6] = 42;
        byte *testData = reinterpret_cast<byte *>(&testDict);
        doRoundtrip(ArgumentList(cstring("a{yy}"), chunk(testData, 10)));
    }
    {
        byte testData[36] = {
            5, // variant signature length
            '(', 'y', 'g', 'd', ')', '\0', // signature: struct of: byte, signature (easiest because
                                           //   its length prefix is byte order independent), double
            0,      // pad to 8-byte boundary for struct
            23,     // the byte
            6, 'i', 'a', '{', 'i', 'v', '}', '\0', // the signature
            0, 0, 0, 0, 0, 0, 0,    // padding to 24 bytes (next 8-byte boundary)
            1, 2, 3, 4, 5, 6, 7, 8, // the double
            20, 21, 22, 23 // the int (not part of the variant)
        };
        doRoundtrip(ArgumentList(cstring("vi"), chunk(testData, 36)));
    }
}

static void test_writerMisuse()
{
    // Array
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginArray(false);
        writer.endArray(); // wrong,  must contain exactly one type
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginArray(true);
        writer.endArray(); // even with no elements it, must contain exactly one type
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginArray(false);
        writer.writeByte(1); // in Writer, calling nextArrayEntry() after beginArray() is optional
        writer.endArray();
        TEST(writer.state() != ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginArray(false);
        writer.nextArrayEntry();    // optional and may not trigger an error
        TEST(writer.state() != ArgumentList::InvalidData);
        writer.endArray(); // wrong, must contain exactly one type
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginArray(false);
        writer.nextArrayEntry();
        writer.writeByte(1);
        writer.writeByte(2);  // wrong, must contain exactly one type
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginArray(true);
        writer.nextArrayEntry();
        writer.beginVariant();
        writer.endVariant(); // empty variants are okay if and only if inside an empty array
        writer.endArray();
        TEST(writer.state() != ArgumentList::InvalidData);
    }
    // Dict
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginDict(false);
        writer.endDict(); // wrong, must contain exactly two types
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginDict(false);
        writer.nextDictEntry();
        writer.writeByte(1);
        writer.endDict(); // wrong, a dict must contain exactly two types
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginDict(false);
        writer.writeByte(1); // in Writer, calling nextDictEntry() after beginDict() is optional
        writer.writeByte(2);
        writer.endDict();
        TEST(writer.state() != ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginDict(false);
        writer.nextDictEntry();
        writer.writeByte(1);
        writer.writeByte(2);
        TEST(writer.state() != ArgumentList::InvalidData);
        writer.writeByte(3); // wrong, a dict contains only exactly two types
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginDict(false);
        writer.nextDictEntry();
        writer.beginVariant(); // wrong, key type must be basic
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    // Variant
    {
        // this and the next are a baseline to make sure that the following test fails for a good reason
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginVariant();
        writer.writeByte(1);
        writer.endVariant();
        TEST(writer.state() != ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginVariant();
        writer.endVariant();
        TEST(writer.state() == ArgumentList::InvalidData);
    }
    {
        ArgumentList arg;
        ArgumentList::Writer writer = arg.beginWrite();
        writer.beginVariant();
        writer.writeByte(1);
        writer.writeByte(2); // wrong, a variant may contain only one or zero single complete types
        TEST(writer.state() == ArgumentList::InvalidData);
    }
}

void addSomeVariantStuff(ArgumentList::Writer *writer)
{
    writer->beginVariant();
        writer->beginVariant();
            writer->beginVariant();
                writer->beginStruct();
                    writer->writeString(cstring("Smoerebroed smoerebroed"));
                    writer->beginStruct();
                        writer->writeString(cstring("Bork bork bork"));
                        writer->beginVariant();
                            writer->beginStruct();
                                writer->writeString(cstring("Quite nesty"));
                                writer->writeObjectPath(cstring("/path/to/object"));
                                writer->writeUint64(234234234);
                                writer->writeByte(2);
                                writer->writeUint64(234234223434);
                                writer->writeUint16(34);
                            writer->endStruct();
                        writer->endVariant();
                        writer->beginStruct();
                            writer->writeByte(34);
                        writer->endStruct();
                    writer->endStruct();
                    writer->writeString(cstring("Another string"));
                writer->endStruct();
            writer->endVariant();
        writer->endVariant();
    writer->endVariant();
}

static void test_complicated()
{
    ArgumentList arg;
    {
        ArgumentList::Writer writer = arg.beginWrite();
        // NeedMoreData-related bugs are less dangerous inside arrays, so we try to provoke one here;
        // the reason for arrays preventing failures is that they have a length prefix which enables
        // and encourages pre-fetching all the array's data before processing *anything* inside the
        // array. therefore no NeedMoreData state happens while really deserializing the array's
        // contents. but we exactly want NeedMoreData while in the middle of deserializing something
        // meaty, specifically variants. see Reader::replaceData().
        addSomeVariantStuff(&writer);

        writer.writeInt64(234234);
        writer.writeByte(115);
        writer.beginVariant();
            writer.beginDict(false);
                writer.writeByte(23);
                writer.beginVariant();
                    writer.writeString(cstring("twenty-three"));
                writer.endVariant();
            writer.nextDictEntry();
                writer.writeByte(83);
                writer.beginVariant();
                writer.writeObjectPath(cstring("/foo/bar/object"));
                writer.endVariant();
            writer.nextDictEntry();
                writer.writeByte(234);
                writer.beginVariant();
                    writer.beginArray(false);
                        writer.writeUint16(234);
                    writer.nextArrayEntry();
                        writer.writeUint16(234);
                    writer.nextArrayEntry();
                        writer.writeUint16(234);
                    writer.endArray();
                writer.endVariant();
            writer.nextDictEntry();
                writer.writeByte(25);
                writer.beginVariant();
                    addSomeVariantStuff(&writer);
                writer.endVariant();
            writer.endDict();
        writer.endVariant();
        writer.writeString("Hello D-Bus!");
        writer.beginArray(false);
            writer.writeDouble(1.567898);
        writer.nextArrayEntry();
            writer.writeDouble(1.523428);
        writer.nextArrayEntry();
            writer.writeDouble(1.621133);
        writer.nextArrayEntry();
            writer.writeDouble(1.982342);
        writer.endArray();
        TEST(writer.state() != ArgumentList::InvalidData);
        writer.finish();
        TEST(writer.state() != ArgumentList::InvalidData);
    }
    doRoundtrip(arg);
}

static void test_alignment()
{
    ArgumentList arg;
    {
        ArgumentList::Writer writer = arg.beginWrite();
        writer.writeByte(123);
        writer.beginArray(false);
        writer.writeByte(64);
        writer.endArray();
        writer.writeByte(123);
        for (int i = 124; i < 150; i++) {
            writer.writeByte(i);
        }

        TEST(writer.state() != ArgumentList::InvalidData);
        writer.finish();
        TEST(writer.state() != ArgumentList::InvalidData);
        doRoundtrip(arg);
    }
    {
        ArgumentList::Writer writer = arg.beginWrite();
        writer.writeByte(123);
        writer.beginStruct();
        writer.writeByte(110);
        writer.endStruct();
        writer.writeByte(200);
        writer.finish();
        doRoundtrip(arg);
    }
}

static void test_arrayOfVariant()
{
    ArgumentList arg;
    // non-empty array
    {
        ArgumentList::Writer writer = arg.beginWrite();
        writer.writeByte(123);
        writer.beginArray(false);
        writer.beginVariant();
        writer.writeByte(64);
        writer.endVariant();
        writer.endArray();
        writer.writeByte(123);

        TEST(writer.state() != ArgumentList::InvalidData);
        writer.finish();
        TEST(writer.state() != ArgumentList::InvalidData);
        doRoundtrip(arg);
    }
    // empty array
    {
        ArgumentList::Writer writer = arg.beginWrite();
        writer.writeByte(123);
        writer.beginArray(true);
        writer.beginVariant();
        writer.endVariant();
        writer.endArray();
        writer.writeByte(123);

        TEST(writer.state() != ArgumentList::InvalidData);
        writer.finish();
        TEST(writer.state() != ArgumentList::InvalidData);
        doRoundtrip(arg);
    }
}

static void test_realMessage()
{
    ArgumentList arg;
    // non-empty array
    {
        ArgumentList::Writer writer = arg.beginWrite();

        writer.writeString(cstring("message"));
        writer.writeString(cstring("konversation"));

        writer.beginArray(true);
        writer.beginVariant();
        writer.endVariant();
        writer.endArray();

        writer.writeString(cstring(""));
        writer.writeString(cstring("&lt;fredrikh&gt; he's never on irc"));

        writer.beginArray(true);
        writer.writeByte(123); // may not show up in the output
        writer.endArray();

        writer.beginArray(true);
        writer.writeString(cstring("dummy, I may not show up in the output!"));
        writer.endArray();

        writer.writeInt32(-1);
        writer.writeInt64(46137372);

        TEST(writer.state() != ArgumentList::InvalidData);
        writer.finish();
        TEST(writer.state() != ArgumentList::InvalidData);
    }
    doRoundtrip(arg);
}

// TODO: test where we compare data and signature lengths of all combinations of zero/nonzero array
//       length and long/short type signature, to make sure that the signature is written but not
//       any data if the array is zero-length.

// TODO test empty dicts, too

int main(int argc, char *argv[])
{
    test_stringValidation();
    test_readerWriterExclusion();
    test_nesting();
    test_roundtrip();
    test_writerMisuse();
    test_complicated();
    test_alignment();
    test_arrayOfVariant();
    test_realMessage();
    // TODO many more misuse tests for Writer and maybe some for Reader
    std::cout << "Passed!\n";
}
